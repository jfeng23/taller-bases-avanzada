﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class ConsultaProductor : UserControl
    {
        public ConsultaProductor()
        {
            InitializeComponent();
            initComboBox();
        }

        private void initComboBox()
        {
            List<DateComboBoxItem> items = new List<DateComboBoxItem>();
            List<productor> productores = DbHelpler.getProductores();
            int counter = productores.Count;
            for (int i = 0; i < counter; i++)
            {
                DateComboBoxItem itemm = new DateComboBoxItem();
                itemm.text = productores.ElementAt(i)._nombre;

                items.Add(itemm);
            }
            comboBox.DisplayMember = "text";
            comboBox.ValueMember = "text";
            comboBox.DataSource = items;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            List<pelicula> pelis = DbHelpler.getPelisPorProductora(comboBox.SelectedValue.ToString());
            pelis = pelis.OrderBy(x => x._duracion).ToList();

            int duracion = 0;
            foreach(pelicula peli in pelis)
            {
                duracion += peli._duracion;
            }
            if(pelis.Count!=0)
            {
                txtPromedio.Text = (duracion / pelis.Count).ToString();
                txtCantidad.Text = pelis.Count.ToString();
                txtCorta.Text = pelis.ElementAt(0)._nombre;
                txtLarga.Text = pelis.Last()._nombre;
            }
            
           
        }
    }
}
