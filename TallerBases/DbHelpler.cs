﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TallerBases
{
    static class DbHelpler
    {
        const string strconn = "mongodb://127.0.0.1:27017";
        const string dbName = "cnblogs";
       


      
        public static bool verificationInsert(string nombre,string collect)
        {

            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs
            MongoDatabase db = server.GetDatabase(dbName);
            var collection = db.GetCollection(collect);
            var all_collection = collection.FindAll();
            List<string> source_name_list = new List<string>();
            foreach (var ttdoc2 in all_collection)
            {
                source_name_list.Add(ttdoc2.AsBsonDocument["_nombre"].ToString());



            }
            if (source_name_list.Contains(nombre))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        internal static List<pelicula> getPelisPorTiempo(int inicial, int final)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            //定义获取“Name”值为“xumingxiang”的查询条件
            return col.Find(Query.And(Query.GTE("_ano",inicial),Query.LTE("_ano",final))).ToList();
        }

        internal static List<pelicula> getPelisPorProductora(string v)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            //定义获取“Name”值为“xumingxiang”的查询条件
            return col.Find(Query.EQ("_compania", v)).ToList();
        }

        internal static List<pelicula> getPelisPorFranquicia(string franquicia)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            //定义获取“Name”值为“xumingxiang”的查询条件
            return col.Find(Query.EQ("_franquicia", franquicia)).ToList();
        }

        internal static bool peliExist(string v)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            //定义获取“Name”值为“xumingxiang”的查询条件
            int cantidad = col.Find(Query.EQ("_nombre", v)).ToList().Count;
            if(cantidad !=0)
            {
                return true;
            }
            return false;
        }

        internal static List<pelicula> getPelis()
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            List<pelicula> result = col.FindAll().ToList();
            return result;
        }

        public static productor getProductorPorNombre(string nombre)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<productor> col = db.GetCollection<productor>("productor");
            //定义获取“Name”值为“xumingxiang”的查询条件
            return col.Find(Query.EQ("_nombre", nombre)).ToList().First();
        }

        public static bool insertProduct(productor product)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs
            MongoDatabase db = server.GetDatabase(dbName);



            //获得Users集合,如果数据库中没有，先新建一个 
            MongoCollection col = db.GetCollection("productor");
            //执行插入操作
            try
            {
                col.Insert<productor>(product);
                return true;
            }
            catch
            {
                return false;
            }


        }

        internal static bool deleteProduct(string nombre)
        {
            //创建数据库链接 
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection col = db.GetCollection("productor");
            //定义获取“Name”值为“xumingxiang”的查询条件 
            var query = new QueryDocument { { "_nombre", nombre } };
            //执行删除操作 
            try
            {
                col.Remove(query);
                return true;
            }
            catch
            {
                return false;
            }
           
        }

        public static bool insertPeli(pelicula peli)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs
            MongoDatabase db = server.GetDatabase(dbName);
          
            

            //获得Users集合,如果数据库中没有，先新建一个 
            MongoCollection col = db.GetCollection("pelicula");
            //执行插入操作
            try
            {
                col.Insert<pelicula>(peli);
                return true;
            }
            catch
            {
                return false;
            }
           

        }

        public static pelicula getPeliculaPorNombre(string nombre)
        {
            //创建数据库链接 
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            //定义获取“Name”值为“xumingxiang”的查询条件
            return col.Find(Query.EQ("_nombre", nombre)).ToList().First();

            //查询全部集合里的数据 var result1 = col.FindAllAs<Users>();
            //查询指定查询条件的第一条数据，查询条件可缺省。 var result2 = col.FindOneAs<Users>();
            //查询指定查询条件的全部数据 var result3 = col.FindAs<Users>(query);
        }

        internal static bool deletePeli(string nombre)
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection col = db.GetCollection("pelicula");
            //定义获取“Name”值为“xumingxiang”的查询条件 
            var query = new QueryDocument { { "_nombre", nombre } };
            //执行删除操作 
            try
            {
                col.Remove(query);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool updateProduc(productor prod)
        {
            // 创建数据库链接 
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<productor> col = db.GetCollection<productor>("productor");
            var query = new QueryDocument { { "_nombre", prod._nombre } };
            productor tmp = col.FindOneAs<productor>(query);
            tmp._ano = prod._ano;
            tmp._web = prod._web;
            try
            {
                var result = col.Save(tmp);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool updatePeli(pelicula peli)
        {
            // 创建数据库链接 
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<pelicula> col = db.GetCollection<pelicula>("pelicula");
            var query = new QueryDocument { { "_nombre", peli._nombre } };
            pelicula tmp = col.FindOneAs<pelicula>(query);
            tmp._actores = peli._actores;
            tmp._ano = peli._ano;
            tmp._compania = peli._compania;
            tmp._director = peli._director;
            tmp._duracion = peli._duracion;
            tmp._franquicia = peli._franquicia;
            tmp._genero = peli._genero;
            tmp._pais = peli._pais;
            try
            {
                var result = col.Save(tmp);
                return true;
            }
            catch
            {
                return false;
            }


        }


        public static List<productor> getProductores()
        {
            MongoServer server = MongoDB.Driver.MongoServer.Create(strconn);
            //获得数据库cnblogs 
            MongoDatabase db = server.GetDatabase(dbName);
            //获取Users集合 
            MongoCollection<productor> col = db.GetCollection<productor>("productor");
            List<productor> result = col.FindAll().ToList();
            return result;
        }
    }
}
