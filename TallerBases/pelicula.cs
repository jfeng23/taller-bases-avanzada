﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TallerBases
{
    class pelicula
    {
        public ObjectId _id;

        public string _nombre { get; set; }

        public string _genero { get; set; }
        public string _director { get; set; }
        public string _franquicia { get; set; }
        
        public string _pais { get; set; }

        public int _ano { get; set; }
        public int _duracion { get; set; }
        public string _compania { get; set; }
        public string[] _actores { get; set; }

    }
}
