﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class AddPeli : Form
    {
        string nombre;
       
        public AddPeli(string condition,string nombre)
        {
            InitializeComponent();
            initComboBox();
            if (condition != "Add")
            {

                this.nombre = nombre;
                loadData(condition);
            }
          
        }

        private void initComboBox()
        {
            List<DateComboBoxItem> items = new List<DateComboBoxItem>();
            List<productor> productores = DbHelpler.getProductores();
            int counter = productores.Count;
            for (int i = 0; i < counter; i++)
            {
                DateComboBoxItem itemm = new DateComboBoxItem();
                itemm.text = productores.ElementAt(i)._nombre;
                
                items.Add(itemm);
            }
            comboBox.DisplayMember = "text";
            comboBox.ValueMember = "text";
            comboBox.DataSource = items;
        }

        private void loadData(string condition)
        {
            if(condition == "Consulta")
            {
                btConfirm.Visible = false;
                btnCancel.Visible = false;
                
            }
            btConfirm.Text = "Actualizar";
            btnCancel.Text = "Eliminar";
            
            pelicula peli = DbHelpler.getPeliculaPorNombre(nombre);
            txtNombre.Text = peli._nombre;
            txtNombre.Enabled = false;
            txtDirector.Text = peli._director;
            txtAno.Text = peli._ano.ToString();
            comboBox.SelectedValue = peli._compania;
            txtDuracion.Text = peli._duracion.ToString();
            txtGenero.Text = peli._genero;
            txtPais.Text = peli._pais;
            txtFraquicia.Text = peli._franquicia;
            string actores= "";
            for(int i = 0; i<peli._actores.Length;i++)
            {
                actores += peli._actores[i] + "\n";
            }
            txtActores.Text = actores;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            string condition = btConfirm.Text.ToString();
            if(condition=="Agregar")
            {
                if (DbHelpler.verificationInsert(txtNombre.Text.ToString(),"pelicula"))
                {
                    pelicula users = new pelicula();
                    users._nombre = txtNombre.Text.ToString();
                    users._actores = txtActores.Text.ToString().Split(',').ToArray();
                    users._ano = Int32.Parse(txtAno.Text.ToString());
                    users._compania = comboBox.SelectedValue.ToString();
                    users._director = txtDirector.Text.ToString();
                    users._genero = txtGenero.Text.ToString();
                    users._duracion = Int32.Parse(txtDuracion.Text.ToString());
                    users._franquicia = txtFraquicia.Text.ToString();
                    users._pais = txtPais.Text.ToString();

                    if (DbHelpler.insertPeli(users))
                    {
                        MessageBox.Show("Pelicula insertado");
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Error,verifique los datos");
                    }

                }
                else
                {
                    MessageBox.Show("Pelicula ya existe en la base de dato");
                }

            }
            else
            {
                pelicula users = new pelicula();
                users._nombre = txtNombre.Text.ToString();
                users._actores = txtActores.Text.ToString().Split(',').ToArray();
                users._ano = Int32.Parse(txtAno.Text.ToString());
                users._compania = comboBox.SelectedValue.ToString();
                users._director = txtDirector.Text.ToString();
                users._genero = txtGenero.Text.ToString();
                users._duracion = Int32.Parse(txtDuracion.Text.ToString());
                users._franquicia = txtFraquicia.Text.ToString();
                users._pais = txtPais.Text.ToString();
                if (DbHelpler.updatePeli(users))
                {
                    MessageBox.Show("Productor Actualizado");
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
          
           
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
            if(btnCancel.Text!="Eliminar")
            {
                this.Hide();
            }
            else
            {
                if (DbHelpler.deletePeli(nombre)) 
                {
                    MessageBox.Show("Pelicula Eliminado");
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
        }
    }
}
