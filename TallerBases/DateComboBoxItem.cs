﻿namespace TallerBases
{
    class DateComboBoxItem
    {
        public string text { get; set; }
        public string value { get; set; }

        public int id { get; set; }
        public DateComboBoxItem()
        {

        }
    }
}