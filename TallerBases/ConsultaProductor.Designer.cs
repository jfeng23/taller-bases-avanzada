﻿namespace TallerBases
{
    partial class ConsultaProductor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.comboBox = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtCantidad = new MetroFramework.Controls.MetroTextBox();
            this.txtLarga = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtCorta = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtPromedio = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.txtPromedio);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.txtCorta);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.txtLarga);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.txtCantidad);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.comboBox);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.metroButton1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(865, 535);
            this.metroPanel1.TabIndex = 7;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.ItemHeight = 23;
            this.comboBox.Location = new System.Drawing.Point(293, 32);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(230, 29);
            this.comboBox.TabIndex = 5;
            this.comboBox.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(209, 42);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(78, 19);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Productora:";
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(613, 29);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(124, 43);
            this.metroButton1.TabIndex = 2;
            this.metroButton1.Text = "Mostrar";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(131, 143);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(136, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Cantidad de Peliculas:";
            // 
            // txtCantidad
            // 
            // 
            // 
            // 
            this.txtCantidad.CustomButton.Image = null;
            this.txtCantidad.CustomButton.Location = new System.Drawing.Point(208, 1);
            this.txtCantidad.CustomButton.Name = "";
            this.txtCantidad.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCantidad.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCantidad.CustomButton.TabIndex = 1;
            this.txtCantidad.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCantidad.CustomButton.UseSelectable = true;
            this.txtCantidad.CustomButton.Visible = false;
            this.txtCantidad.Lines = new string[0];
            this.txtCantidad.Location = new System.Drawing.Point(293, 143);
            this.txtCantidad.MaxLength = 32767;
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.PasswordChar = '\0';
            this.txtCantidad.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCantidad.SelectedText = "";
            this.txtCantidad.SelectionLength = 0;
            this.txtCantidad.SelectionStart = 0;
            this.txtCantidad.ShortcutsEnabled = true;
            this.txtCantidad.Size = new System.Drawing.Size(230, 23);
            this.txtCantidad.TabIndex = 7;
            this.txtCantidad.UseSelectable = true;
            this.txtCantidad.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCantidad.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLarga
            // 
            // 
            // 
            // 
            this.txtLarga.CustomButton.Image = null;
            this.txtLarga.CustomButton.Location = new System.Drawing.Point(208, 1);
            this.txtLarga.CustomButton.Name = "";
            this.txtLarga.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLarga.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLarga.CustomButton.TabIndex = 1;
            this.txtLarga.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLarga.CustomButton.UseSelectable = true;
            this.txtLarga.CustomButton.Visible = false;
            this.txtLarga.Lines = new string[0];
            this.txtLarga.Location = new System.Drawing.Point(293, 219);
            this.txtLarga.MaxLength = 32767;
            this.txtLarga.Name = "txtLarga";
            this.txtLarga.PasswordChar = '\0';
            this.txtLarga.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLarga.SelectedText = "";
            this.txtLarga.SelectionLength = 0;
            this.txtLarga.SelectionStart = 0;
            this.txtLarga.ShortcutsEnabled = true;
            this.txtLarga.Size = new System.Drawing.Size(230, 23);
            this.txtLarga.TabIndex = 9;
            this.txtLarga.UseSelectable = true;
            this.txtLarga.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLarga.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(150, 219);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(117, 19);
            this.metroLabel3.TabIndex = 8;
            this.metroLabel3.Text = "Pelicula mas larga:";
            // 
            // txtCorta
            // 
            // 
            // 
            // 
            this.txtCorta.CustomButton.Image = null;
            this.txtCorta.CustomButton.Location = new System.Drawing.Point(208, 1);
            this.txtCorta.CustomButton.Name = "";
            this.txtCorta.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCorta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCorta.CustomButton.TabIndex = 1;
            this.txtCorta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCorta.CustomButton.UseSelectable = true;
            this.txtCorta.CustomButton.Visible = false;
            this.txtCorta.Lines = new string[0];
            this.txtCorta.Location = new System.Drawing.Point(293, 292);
            this.txtCorta.MaxLength = 32767;
            this.txtCorta.Name = "txtCorta";
            this.txtCorta.PasswordChar = '\0';
            this.txtCorta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCorta.SelectedText = "";
            this.txtCorta.SelectionLength = 0;
            this.txtCorta.SelectionStart = 0;
            this.txtCorta.ShortcutsEnabled = true;
            this.txtCorta.Size = new System.Drawing.Size(230, 23);
            this.txtCorta.TabIndex = 11;
            this.txtCorta.UseSelectable = true;
            this.txtCorta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCorta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(149, 292);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(118, 19);
            this.metroLabel4.TabIndex = 10;
            this.metroLabel4.Text = "Pelicula mas corta:";
            // 
            // txtPromedio
            // 
            // 
            // 
            // 
            this.txtPromedio.CustomButton.Image = null;
            this.txtPromedio.CustomButton.Location = new System.Drawing.Point(208, 1);
            this.txtPromedio.CustomButton.Name = "";
            this.txtPromedio.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPromedio.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPromedio.CustomButton.TabIndex = 1;
            this.txtPromedio.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPromedio.CustomButton.UseSelectable = true;
            this.txtPromedio.CustomButton.Visible = false;
            this.txtPromedio.Lines = new string[0];
            this.txtPromedio.Location = new System.Drawing.Point(293, 372);
            this.txtPromedio.MaxLength = 32767;
            this.txtPromedio.Name = "txtPromedio";
            this.txtPromedio.PasswordChar = '\0';
            this.txtPromedio.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPromedio.SelectedText = "";
            this.txtPromedio.SelectionLength = 0;
            this.txtPromedio.SelectionStart = 0;
            this.txtPromedio.ShortcutsEnabled = true;
            this.txtPromedio.Size = new System.Drawing.Size(230, 23);
            this.txtPromedio.TabIndex = 13;
            this.txtPromedio.UseSelectable = true;
            this.txtPromedio.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPromedio.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(147, 372);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(120, 19);
            this.metroLabel5.TabIndex = 12;
            this.metroLabel5.Text = "Tiempo Promedio:";
            // 
            // ConsultaProductor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel1);
            this.Name = "ConsultaProductor";
            this.Size = new System.Drawing.Size(865, 535);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTextBox txtCantidad;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox comboBox;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox txtPromedio;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtCorta;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtLarga;
        private MetroFramework.Controls.MetroLabel metroLabel3;
    }
}
