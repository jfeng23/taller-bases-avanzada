﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class ConsultaPorAno : UserControl
    {
        public ConsultaPorAno()
        {
            InitializeComponent();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if(txtInicial.Text !="" && txtFinal.Text!="")
            {
                int inicial = Int32.Parse(txtInicial.Text.ToString());
                int final = Int32.Parse(txtFinal.Text.ToString());
                List<pelicula> pelis = DbHelpler.getPelisPorTiempo(inicial, final);
                createTable(pelis);
            }
            
        }
        private void createTable(List<pelicula> pelis)
        {
            List<pelicula> productores = pelis;
            DataTable table = new DataTable();
            table.Columns.Add("Nombre".ToString());
            table.Columns.Add("Genero".ToString());
            table.Columns.Add("Ano".ToString());

            foreach (pelicula pro in productores)
            {
                DataRow dr = table.NewRow();
                dr["Nombre"] = pro._nombre;
                dr["Genero"] = pro._genero;
                dr["Ano"] = pro._ano;
                table.Rows.Add(dr);
            }
            metroGrid1.DataSource = table;
            int count = metroGrid1.Columns.Count;
            for (int i = 0; i < count; i++)
            {
                metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Id = (metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString());
            AddPeli uc = new AddPeli("Consulta", Id);
            uc.Show();
        }
    }
}
