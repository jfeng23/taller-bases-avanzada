﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class AdmiPeli : UserControl
    {
        public AdmiPeli()
        {
            InitializeComponent();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            List<pelicula> productores = DbHelpler.getPelis();
            DataTable table = new DataTable();
            table.Columns.Add("Nombre".ToString());
            table.Columns.Add("Genero".ToString());
            table.Columns.Add("Ano".ToString());

            foreach (pelicula pro in productores)
            {
                DataRow dr = table.NewRow();
                dr["Nombre"] = pro._nombre;
                dr["Genero"] = pro._genero;
                dr["Ano"] = pro._ano;
                table.Rows.Add(dr);
            }
            metroGrid1.DataSource = table;
            int count = metroGrid1.Columns.Count;
            for (int i = 0; i < count; i++)
            {
                metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            AddPeli uc = new AddPeli("Add","");
            uc.Show();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Id = (metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString());
            AddPeli uc = new AddPeli("edit", Id);
            uc.Show();
        }
    }
}
