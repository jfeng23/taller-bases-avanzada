﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class AddProductor : Form
    {
        string nombre;
        public AddProductor(string condition, string nombre)
        {
            InitializeComponent();
            if(condition == "edit")
            {
                this.nombre = nombre;
                loadData();
                btConfirm.Text = "Actualizar";
                btnCancel.Text = "Eliminar";
            }
        }

        private void loadData()
        {
            productor produc = DbHelpler.getProductorPorNombre(nombre);
            txtNombre.Text = produc._nombre;
            txtNombre.Enabled = false;
            txtAno.Text =produc._ano.ToString();
            txtWebsite.Text = produc._web;
        }

        private void btConfirm_Click(object sender, EventArgs e)
        {
            if(btConfirm.Text =="Agregar")
            {
                if (DbHelpler.verificationInsert(txtNombre.Text.ToString(), "productor"))
                {
                    productor product = new productor();
                    product._nombre = txtNombre.Text.ToString();
                    product._ano = Int32.Parse(txtAno.Text.ToString());
                    product._web = (txtWebsite.Text.ToString());

                    if (DbHelpler.insertProduct(product))
                    {
                        MessageBox.Show("Productor insertado");
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Error");
                    }
                }
                else
                {
                    MessageBox.Show("Ya existe");
                }
           
            }
            else
            {
                productor product = new productor();
                product._nombre = txtNombre.Text.ToString();
                product._ano = Int32.Parse(txtAno.Text.ToString());
                product._web = (txtWebsite.Text.ToString());
                if(DbHelpler.updateProduc(product))
                {
                    MessageBox.Show("Productor Actualizado");
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(btnCancel.Text=="Cancelar")
            {
                this.Hide();
            }
            else
            {
                if(DbHelpler.deleteProduct(nombre))
                {
                    MessageBox.Show("Productor Eliminado");
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
        }
    }
}
