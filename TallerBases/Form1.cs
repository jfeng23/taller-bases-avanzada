﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddPeli uc = new AddPeli("Add","");
            uc.Show();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            ContextPanel.Controls.Clear();
            AdmiPeli uc = new AdmiPeli();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {

            ContextPanel.Controls.Clear();
            admiProduc uc = new admiProduc();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {
            ContextPanel.Controls.Clear();
            ConsultaTitulo uc = new ConsultaTitulo();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            ContextPanel.Controls.Clear();
            ConsultaFranquicia uc = new ConsultaFranquicia();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }

        private void metroTile5_Click(object sender, EventArgs e)
        {
            ContextPanel.Controls.Clear();
            ConsultaPorAno uc = new ConsultaPorAno();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }

        private void metroTile6_Click(object sender, EventArgs e)
        {
            ContextPanel.Controls.Clear();
            ConsultaPorProduct uc = new ConsultaPorProduct();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }

        private void metroTile7_Click(object sender, EventArgs e)
        {
            ContextPanel.Controls.Clear();
            ConsultaProductor uc = new ConsultaProductor();
            uc.Dock = DockStyle.Fill;
            ContextPanel.Controls.Add(uc);
        }
    }
}
