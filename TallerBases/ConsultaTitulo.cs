﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class ConsultaTitulo : UserControl
    {
        public ConsultaTitulo()
        {
            InitializeComponent();
            start();
        }

        private void start()
        {
            txtActores.Enabled = false;
            txtAno.Enabled = false;
            txtCompania.Enabled = false;
            txtDirector.Enabled = false;
            txtDuracion.Enabled = false;
            txtFraquicia.Enabled = false;
            txtGenero.Enabled = false;
            txtPais.Enabled = false;

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (DbHelpler.peliExist(txtNombre.Text.ToString()))
            {
                loadData(txtNombre.Text.ToString());
            }
            else
            {
                MessageBox.Show("Pelicula no existe");
            }
        }

        private void loadData(string nombre)
        {

            pelicula peli = DbHelpler.getPeliculaPorNombre(nombre);
            txtNombre.Text = peli._nombre;
           // txtNombre.Enabled = false;
            txtDirector.Text = peli._director;
            txtAno.Text = peli._ano.ToString();
            txtCompania.Text = peli._compania;
            txtDuracion.Text = peli._duracion.ToString();
            txtGenero.Text = peli._genero;
            txtPais.Text = peli._pais;
            txtFraquicia.Text = peli._franquicia;
            string actores = "";
            for (int i = 0; i < peli._actores.Length; i++)
            {
                actores += peli._actores[i] + "\n";
            }
            txtActores.Text = actores;

        }
    }
}
