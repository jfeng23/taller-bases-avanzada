﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class ConsultaPorProduct : UserControl
    {
        public ConsultaPorProduct()
        {
            InitializeComponent();
            initComboBox();
        }

        private void initComboBox()
        {
            List<DateComboBoxItem> items = new List<DateComboBoxItem>();
            List<productor> productores = DbHelpler.getProductores();
            int counter = productores.Count;
            for (int i = 0; i < counter; i++)
            {
                DateComboBoxItem itemm = new DateComboBoxItem();
                itemm.text = productores.ElementAt(i)._nombre;

                items.Add(itemm);
            }
            comboBox.DisplayMember = "text";
            comboBox.ValueMember = "text";
            comboBox.DataSource = items;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            List<pelicula> pelis = DbHelpler.getPelisPorProductora(comboBox.SelectedValue.ToString());
            createTable(pelis);
        }

        private void createTable(List<pelicula> pelis)
        {
            List<pelicula> productores = pelis;
            DataTable table = new DataTable();
            table.Columns.Add("Nombre".ToString());
            table.Columns.Add("Genero".ToString());
            table.Columns.Add("Ano".ToString());

            foreach (pelicula pro in productores)
            {
                DataRow dr = table.NewRow();
                dr["Nombre"] = pro._nombre;
                dr["Genero"] = pro._genero;
                dr["Ano"] = pro._ano;
                table.Rows.Add(dr);
            }
            metroGrid1.DataSource = table;
            int count = metroGrid1.Columns.Count;
            for (int i = 0; i < count; i++)
            {
                metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
    }
}
