﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerBases
{
    public partial class admiProduc : UserControl
    {
        public admiProduc()
        {
            InitializeComponent();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            loadData();
        }

        private void loadData()
        {
            List<productor> productores = DbHelpler.getProductores();
            DataTable table = new DataTable();
            table.Columns.Add("Nombre".ToString());
            table.Columns.Add("Ano".ToString());
            table.Columns.Add("Web".ToString());

            foreach(productor pro in productores)
            {
                DataRow dr = table.NewRow();
                dr["Nombre"] = pro._nombre;
                dr["Ano"] = pro._ano;
                dr["Web"] = pro._web;
                table.Rows.Add(dr);
            }
            metroGrid1.DataSource = table;
            int count = metroGrid1.Columns.Count;
            for (int i = 0; i < count; i++)
            {
                metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            AddProductor uc = new AddProductor("Add","");
            uc.Show();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Id = (metroGrid1.Rows[e.RowIndex].Cells[0].Value.ToString());
            AddProductor uc = new AddProductor("edit", Id);
            uc.Show();
        }
    }
}
